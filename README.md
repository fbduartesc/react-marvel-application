# React Marvel

Project application marvel characters in React, Redux, React-router and @testing-library/react for tests.

# Get started

* $ git clone https://gitlab.com/fbduartesc/react-marvel-application.git
* $ cd react-marvel-application
* $ yarn install
* $ yarn startApp