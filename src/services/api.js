import axios from 'axios';
import CryptoJS from 'crypto-js/md5';

const api = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL
});

const apikey = process.env.REACT_APP_API_KEY;
const apiPrivateKey = process.env.REACT_APP_API_PRIVATE_KEY;
const ts = Math.floor(Date.now() / 1000);
const hash = CryptoJS(ts + apiPrivateKey + apikey).toString();
const paramsApi = {
    apikey,
    ts,
    hash
}

api.interceptors.request.use((config) => {
    config.params = {...config.params, ...paramsApi};
    return config;
});

export default api;