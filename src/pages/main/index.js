import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { formatUrlImagem, getAllLocalStorage } from '../../utils';
import { client } from '../../utils/client';
import gql from 'graphql-tag';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CardGroup from 'react-bootstrap/CardGroup';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

import { connect } from 'react-redux';
import { characters, characterInfo, page } from '../../store/actions';

import Search from '../../components/Search';

import './style.css';

function Item({ character }) {
    return (
        <div className="col-md-2">
            <Link className="link"
                title={character.name}
                to={`/characters/${character.id}`}>
                <img className="imgFormat"
                    alt={character.name}
                    title={character.name}
                    src={formatUrlImagem(character)} />
                <Card.Body>
                    <Card.Title className="justify-content-md-center">{character.name}</Card.Title>
                </Card.Body>
            </Link>
        </div>
    );
}

function List({ characters }) {
    if (!characters) {
        return null;
    }

    if (!characters.length) {
        return (
            <Row className="justify-content-md-center">
                <h5>Nenhum resultado encontrado com o termo informado</h5>
            </Row>
        );
    }

    return (
        <CardGroup>
            {characters.map(character => (
                <Item key={character.id} character={character} />
            ))}
        </CardGroup>
    );
}

class Main extends Component {

    componentDidMount() {
        this.loadCharacters();
    }

    loadCharacters = async (page = 1, params = {}) => {

        const query = gql`
        query Characters ($params: PaginationInput){
            data: characters (params: $params) {                
                count
                limit
                offset
                total
                results {
                    id
                    name
                    description
                    thumbnail {
                        path
                        extension
                    }
                }
            }
        }
        `;

        const offset = (page === 1) ? 0 : (page - 1) * 20;
        params = {
            params: {
                offset,
                ...params
            }
        };

        const response = await client.query({ query, variables: params });
        const { results, ...characterInfo } = response.data.data;
        characterInfo.page = page;
        characterInfo.offset = offset;
        characterInfo.pages = Math.ceil(characterInfo.total / characterInfo.count);
        this.props.setCharacters(results);
        this.props.setCharacterInfo(characterInfo);
        this.props.setPage(page);
    };

    prevPage = () => {
        const { page } = this.props;
        if (page === 1) return;
        const pageNumber = page - 1;
        this.loadCharacters(pageNumber);
    }

    nextPage = () => {
        const { page, characterInfo } = this.props;
        if (page === characterInfo.pages) return;
        const pageNumber = page + 1;
        this.loadCharacters(pageNumber);
    }

    render() {
        const characters = getAllLocalStorage(this.props.characters);
        const {characterInfo} = this.props;
        return (
            <div>
                <Row className="characters">
                    <Col><h2>Lista de personagens Marvel</h2></Col>
                </Row>
                <hr />
                <Search isSearching={this.loadCharacters} />
                <hr />
                <List characters={characters} />
                <hr/>
                <Row className="pagination justify-content-md-center">
                    <Col md={2}>
                        <Button variant="dark"
                            title="Anterior"
                            disabled={characterInfo.page === 1}
                            data-testid='btnBuscar'
                            onClick={this.prevPage}>Anterior</Button>
                    </Col>
                    <Col md={{ span: 2, offset: 6 }}> 
                        <Button variant="dark"
                            title="Próximo"
                            disabled={characterInfo.page === characterInfo.pages}
                            data-testid='btnBuscar'
                            onClick={this.nextPage}>Próximo</Button>
                    </Col>
                </Row>
            </div>
        );
    };
};

const mapStateToProps = state => {
    return {
        characters: state.characters,
        characterInfo: state.characterInfo,
        page: state.page,
        search: state.search
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setCharacters: (character) => dispatch(characters(character)),
        setCharacterInfo: (info) => dispatch(characterInfo(info)),
        setPage: (pageNumber) => dispatch(page(pageNumber))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);