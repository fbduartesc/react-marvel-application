import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Series from '../../components/Series';
import FormContainer from '../../components/FormContainer';

import { formatUrlImagem, getItemLocalStorage } from '../../utils';
import { client } from '../../utils/client';
import gql from 'graphql-tag';

import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Media from 'react-bootstrap/Media';
import Button from 'react-bootstrap/Button';

import { connect } from 'react-redux';
import { characters, series } from '../../store/actions';

import './style.css'

class Character extends Component {

  componentDidMount() {
    this.loadCharacter();
  }

  loadCharacter = async () => {

    const query = gql`
      query Character ($id: Int!){
        character (id: $id) {
            id
            name
            thumbnail {
                path
                extension
            }
        }
    }`;

    const querySerie = gql`
        query Series ($id: Int!){
            series (id: $id) {
                id
                title
                description
                thumbnail {
                    path
                    extension
                }
            }
        }`;

    const { id } = this.props.match.params;


    const response = await client.query({ query, variables: { id } });
    const { character: results } = response.data;
    this.setCharacter(results);

    const responseSeries = await client.query({ query: querySerie, variables: { id } });
    const resultSeries = responseSeries.data.series;
    this.props.setSeries(resultSeries);
  }

  setCharacter = (data) => {
    this.props.setCharacters(data);
  }

  render() {

    const { characters, series } = this.props;    
    const firstCharacter = [...characters].shift();
    const character = { ...firstCharacter, ...getItemLocalStorage(firstCharacter.id) };
    
    return (
      <div>
        <Row>
        </Row>
        <Card className="card-character">
          <Card.Header>
            <Link className="link"
              title="Voltar para página inicial"
              to={'/'}>
              <Button variant="dark">Voltar</Button>
            </Link>
          </Card.Header>
          <Card.Body>
            <Media>
              <img
                width={300}
                height={300}
                className="mr-3"
                src={formatUrlImagem(character)}
                alt={character.name}
                title={character.name}
              />
              <Media.Body>
                <h2>{character.name}</h2>
                <FormContainer details={character} setChange={this.setCharacter} />
              </Media.Body>
            </Media>
          </Card.Body>
        </Card>
        <Series listSeries={series} />
      </div>
    );
  }
};

const mapStateToProps = state => {
  return {
    characters: state.characters,
    series: state.series
  }
}
const mapDispatchToProps = dispatch => {
  return {
    setCharacters: (character) => dispatch(characters(character)),
    setSeries: (serie) => dispatch(series(serie))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Character);
