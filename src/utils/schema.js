import { makeExecutableSchema } from 'graphql-tools';
import api from '../services/api';

const typeDefs = `
  type Character {
    id: Int!
    name: String!
    description: String    
    thumbnail: Thumbnail
  }  

  input PaginationInput {
      page: Int
      offset: Int
      params: String
      name: String
  }

  type RequestListPage {
      count: Int!    
      limit: Int!
      offset: Int!
      total: Int!
      results: [Character]
  }

  type Series {
    id: Int!
    title: String!
    description: String
    thumbnail: Thumbnail
  }

  type Thumbnail {
    extension: String
    path: String
  }
  
  type Query {
    series (id:Int!): [Series]
    character (id:Int!): [Character]
    characters (params: PaginationInput): RequestListPage
  }
  
  schema {
    query: Query
  }
`;

const characters = (root, params) => api.get('/characters', params).then(res => res.data.data);

const character = (root, {id}) => api.get(`/characters/${id}`).then(res => res.data.data.results);

const series = (root, {id}) => api.get(`/characters/${id}/series`).then(res => res.data.data.results)

const resolvers = {
  Query: {
    character,
    characters,
    series
  }
}

export const schema = makeExecutableSchema({ typeDefs, resolvers })