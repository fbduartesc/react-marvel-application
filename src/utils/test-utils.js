import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import { characters, characterInfo, page, series, search, detailCharacter } from '../store/reducer';

const initialState = {
    characters: [],
    characterInfo: {},
    page: 1,
    series: [],
    search: '',
    detailCharacter: {}
  };
  
  const combined = combineReducers({
    characters, 
    characterInfo,
    page,
    series,
    search,
    detailCharacter
  });

function render(
  ui,
  {
    store = createStore(combined, initialState),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

export * from '@testing-library/react'

export { render }