export const formatUrlImagem = (character) => {
    const { thumbnail } = character;
    const urlBase = `${thumbnail.path.replace('http://', 'https://')}.${thumbnail.extension}`;
    return urlBase;
};

export const getItemLocalStorage = (id) => {
    const items = JSON.parse(localStorage.getItem('characters')) || [];
    const indexItem = items.findIndex(item => item.id === id);
    return items[indexItem];
};

export const getAllLocalStorage = (data) => {
    const items = JSON.parse(localStorage.getItem('characters')) || [];    
    return data.map(lista => {
        let item = items.find(itemLocalStorage => itemLocalStorage.id === lista.id);
        return item ? { ...lista, ...item } : lista;
    });
};