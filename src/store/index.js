import { createStore, combineReducers } from "redux";
import { characters, characterInfo, page, series, search, detailCharacter } from './reducer';

const initialState = {
  characters: [],
  characterInfo: {},
  page: 1,
  series: [],
  search: '',
  detailCharacter: {}
};

const combined = combineReducers({
  characters, 
  characterInfo,
  page,
  series,
  search,
  detailCharacter
});

const store = createStore(combined, initialState);

export default store;