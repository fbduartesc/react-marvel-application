import { CHARACTERS, CHARACTERINFO, PAGE, SERIES, SEARCH, DETAILCHARACTER } from './actions';

export const characters = (state = [], action) => {
    switch (action.type) {
        case CHARACTERS:
            return action.payload;
        default:
            return state;
    }
};

export const series = (state = [], action) => {
    switch (action.type) {
        case SERIES:            
            return action.payload;
        default:
            return state;
    }
};

export const characterInfo = (state = {}, action) => {
    switch (action.type) {
        case CHARACTERINFO:
            return action.payload;
        default:
            return state;
    }
};

export const page = (state = 0, action) => {
    switch (action.type) {
        case PAGE:
            return action.payload;
        default:
            return state;
    }
};

export const search = (state = '', action) => {
    switch (action.type) {
        case SEARCH:
            return action.payload;
        default:
            return state;
    }
};

export const detailCharacter = (state = {}, action) => {
    switch (action.type) {
        case DETAILCHARACTER:
            return action.payload;
        default:
            return state;
    }
};