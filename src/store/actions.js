export const CHARACTERS = 'CHARACTERS';
export const characters = (character) => {
    return {
        type: CHARACTERS,
        payload: character
    };
};

export const SERIES = 'SERIES';
export const series = (serie) => {    
    return {
        type: SERIES,
        payload: serie
    };
};

export const DETAILCHARACTER = 'DETAILCHARACTER';
export const detailCharacter = (detail) => {
    return {
        type: DETAILCHARACTER,
        payload: detail
    };
};

export const CHARACTERINFO = 'CHARACTERINFO';
export const characterInfo = (info) => {
    return {
        type: CHARACTERINFO,
        payload: info
    };
};

export const PAGE = 'PAGE';
export const page = (page) => {
    return {
        type: PAGE,
        payload: page
    };
};

export const SEARCH = 'SEARCH';
export const search = (search) => {
    return {
        type: SEARCH,
        payload: search
    };
};