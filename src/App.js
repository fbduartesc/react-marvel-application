import React from 'react';
import { Provider } from "react-redux";
import Container from 'react-bootstrap/Container';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import store from "./store";
import Routes from './routes';

import Header from './components/Header';

const App = () => (
  <Provider store={store}>
    <Header />
    <Container fluid>
      <Row>
        <Col>
          <Routes />
        </Col>
      </Row>
    </Container>    
  </Provider>
);

export default App;
