import React from 'react';
import { render, fireEvent, screen } from '../../utils/test-utils'

import FormContainer from './index';

test('Deve ter label com valor "Nome"', () => {
    const character = {id: 1, name: '3-D Man'};
    const { getByText } = render(<FormContainer details={character}/>);
    expect(getByText('Nome')).toBeInTheDocument();
});

test('Deve ter label com valor "Descrição"', () => {
    const character = {id: 1, name: '3-D Man'};
    const { getByText } = render(<FormContainer details={character}/>);
    expect(getByText('Descrição')).toBeInTheDocument();
});

test('Deve ter pressionado Enter no campo "Nome"', () => {
    const handleSubmit = jest.fn();
    const character = {id: 1, name: '3-D Man'};
    const { getByTestId } = render(<FormContainer details={character} setChange={handleSubmit}/>);
    fireEvent.change(getByTestId('idName'), {
        target: { value: '3-D Man alterado' }
    });
    expect(handleSubmit.mock.calls.length).toBe(1);
});

test('Deve ter pressionado Enter no campo "Descrição"', () => {
    const handleSubmit = jest.fn();
    const character = {id: 1, name: '3-D Man', description: ''};
    const { getByTestId } = render(<FormContainer details={character} setChange={handleSubmit}/>);
    fireEvent.change(getByTestId('idDescription'), {
        target: { value: 'Descrição alterada' }
    });
    expect(handleSubmit.mock.calls.length).toBe(1);
});