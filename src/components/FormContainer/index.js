import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import { connect } from 'react-redux';
import { detailCharacter } from '../../store/actions';

class FormContainer extends Component {

  handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
    }
  }

  onChange = (event) => {
    this.setChangeCharacter(event);
    this.setLocalStorage(event);
  }

  setLocalStorage = (event) => {
    const { id, name, description } = this.props.details;
    const items = JSON.parse(localStorage.getItem('characters')) || [];
    const indexItem = items.findIndex(item => item.id === id);
    const itemStorage = {
      id,
      name,
      description,
      ...{ [event.target.name]: event.target.value }
    }
    if (indexItem >= 0) {
      items[indexItem] = {...items[indexItem], ...itemStorage};
    }else{
      items.push(itemStorage);
    }
    localStorage.setItem('characters', JSON.stringify(items));
  };

  setChangeCharacter = (event) => {
    const { details } = this.props;
    this.props.setChange([{ ...details, ...{ [event.target.name]: event.target.value } }]);
  };

  render() {
    const { details } = this.props;
    return (
      <Form>
        <Form.Group controlId="formDetailName">
          <Form.Label>Nome</Form.Label>
          <Form.Control type="text"
            name="name"
            data-testid="idName"
            value={details.name}
            onChange={this.onChange}
            onKeyPress={this.handleKeyDown} />
        </Form.Group>
        <Form.Group controlId="formDetailDescription">
          <Form.Label>Descrição</Form.Label>
          <Form.Control as="textarea"
            rows="3"
            name="description"
            data-testid="idDescription"
            value={details.description}
            onChange={this.onChange}
            onKeyPress={this.handleKeyDown} />
        </Form.Group>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    detailCharacter: state.detailCharacter
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setDetailCharacter: (detail) => dispatch(detailCharacter(detail))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);