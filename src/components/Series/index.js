import React, { Component } from 'react';
import {formatUrlImagem} from '../../utils';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';

import CardGroup from 'react-bootstrap/CardGroup';
import Card from 'react-bootstrap/Card';

import './style.css';

function ShowSeries(props) {
    if (!props.series.length) {
        return (
            <Card className="card-serie">
                <Row className="justify-content-md-center">
                    <h3>Séries</h3>
                </Row>
                <hr/>
                <Row>
                    <Col>
                        <h5>Não há nenhuma série por aqui</h5>
                    </Col>
                </Row>
            </Card>
        );
    }

    return (
        <Card className="card-serie">
            <Row className="justify-content-md-center">
                <h3>Séries <Badge variant="secondary">{props.series.length}</Badge></h3>                
            </Row>
            <hr/>
            <CardGroup>
                {props.series.map(serie => (
                    <div className="col-md-2" key={serie.id}>
                        <img className="imgFormat"
                            alt={serie.title}
                            title={serie.title}
                            src={formatUrlImagem(serie)} />
                        <Card.Body>
                            <Card.Title>{serie.title}</Card.Title>
                        </Card.Body>
                    </div>
                ))}
            </CardGroup>
        </Card>
    );
}

class Series extends Component {

    render() {
        const series = this.props.listSeries;
        return (
            <ShowSeries series={series} />
        )
    }
}

export default Series;