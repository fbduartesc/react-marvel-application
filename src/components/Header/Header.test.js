import React from 'react';
import { render, fireEvent, screen } from '../../utils/test-utils'

import Header from './index';

test('Deve ter a logo da marvel', () => {
    const { getByTestId } = render(<Header />);
    expect(getByTestId('imgMarvel')).toBeInTheDocument();
    expect(getByTestId('imgMarvel')).toHaveAttribute('src');
    expect(getByTestId('imgMarvel').title).toBe('Marvel');
});

test('Deve ter a logo do react', () => {
    const { getByTestId } = render(<Header />);
    expect(getByTestId('imgReact')).toBeInTheDocument();
    expect(getByTestId('imgReact')).toHaveAttribute('src');
    expect(getByTestId('imgReact').title).toBe('React');
});