import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Figure from 'react-bootstrap/Figure';

import logoMarvel from './logo-marvel.png';
import logoReact from './logo-react.png';

const Header = () => (
  <Navbar expand="lg" bg="dark" variant="dark">    
      <Figure.Image
        data-testid="imgMarvel"
        width={120}
        className="justify-content-right"
        alt="Marvel"
        title="Marvel"
        src={logoMarvel} />
      <Figure.Image
        data-testid="imgReact"
        width={40}
        alt="React"
        title="React"
        src={logoReact} />    
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  </Navbar>
);

export default Header;