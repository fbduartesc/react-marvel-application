import React from 'react';
import { render, fireEvent, screen } from '../../utils/test-utils'

import Search from './index';

test('Deve ter placeholder e nome do botão chamado "Buscar"', () => {
    const { getByText, getByPlaceholderText } = render(<Search />);
    expect(getByPlaceholderText('Ex: 3-D Man')).toBeInTheDocument();
    expect(getByText('Buscar')).toBeInTheDocument();
});

test('Deve carregar valor default na busca', () => {
    const { getByPlaceholderText } = render(<Search />);
    expect(getByPlaceholderText('Ex: 3-D Man')).toHaveValue('');
});

test('Deve carregar um valor para a busca', () => {
    const { getByPlaceholderText, getByText } = render(<Search />);
    fireEvent.change(getByPlaceholderText('Ex: 3-D Man'), {
        target: { value: '3-D Man' }
    });
    expect(getByPlaceholderText('Ex: 3-D Man')).toHaveValue('3-D Man');
});

test('Deve estar habilitado botão de buscar', () => {
    const { getByTestId } = render(<Search />);
    expect(getByTestId('btnBuscar')).not.toHaveAttribute('disabled')
});

describe('Ao clicar no botão buscar', () => {
    test('Deve atualizar estado', () => {
        const handleSubmit = jest.fn();
        const { getByPlaceholderText, getByTestId } = render(<Search isSearching={handleSubmit} />);
        fireEvent.change(getByPlaceholderText('Ex: 3-D Man'), {
            target: { value: '3-D Man' }
        });
        fireEvent.click(getByTestId('btnBuscar'));
        expect(handleSubmit).toHaveBeenCalledWith(1, { name: '3-D Man' });
    });
});