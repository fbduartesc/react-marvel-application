import React, { Component } from 'react';

import { connect } from 'react-redux';
import { search } from '../../store/actions';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';

import './style.css';

class Search extends Component {

    handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            this.updateCharacter();
        }
        
    }

    updateCharacter = async () => {
        const params = {
            name: this.props.search
        };
        if (!this.props.search) {
            delete params.name;
        }
        await this.props.isSearching(1, params);
    }

    handleChange = (event) => {
        this.props.setSearch(event.target.value);
    };

    render() {
        return (
            <Form inline className="search">
                <FormControl type="text" 
                             onKeyPress={this.handleKeyDown}
                             onChange={ this.handleChange }
                             placeholder="Ex: 3-D Man"                             
                             className="mr-sm-2" />
                <Button variant="dark" 
                        data-testid='btnBuscar'                        
                        onClick={this.updateCharacter}>Buscar</Button>
            </Form>
        )
    }
};

const mapStateToProps = state => {
    return {
        search: state.search
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setSearch: (term) => dispatch(search(term))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);